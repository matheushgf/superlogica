# Instruções de Instalação do projeto Laravel+Composer:
- Baixar Composer (última versão), selecionando a opção de adicionar o PHP ao PATH;
- Copiar pasta do projeto para servidor PHP;
- No console ou terminal, navegar até a pasta do projeto;
- Inserir comando: "composer install"
- Em config/database, configurar informações de acesso ao Banco de Dados.
- Acessar http://localhost/superlogica/public/

# URLs:
- Tela de cadastro (/usuario/cadastro);
- Listagem de todos os usuários (/usuario/lista)
- Busca e exibição de usuário por ID (/usuario/buscaPorId/{id})
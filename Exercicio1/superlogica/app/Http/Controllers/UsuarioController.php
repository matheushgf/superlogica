<?php

namespace App\Http\Controllers;

use App\Models\Usuario;
use App\Models\HistoricoUsuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;
use Illuminate\Support\Facades\Hash;

class UsuarioController extends Controller{

    public function cadastro(){
        return view('usuario.cadastro');
    }

    /**
     * Lista todos os usuários.
     *
     * @return \Illuminate\Http\Response
     */
    public function lista(){
        return Usuario::with('historico')->orderBy()->get();
    }

    /**
     * Lista usuário por ID
     *
     * @return \Illuminate\Http\Response
     */
    public function buscaPorId($id){
        return Usuario::with('ultimoHistorico')->findOrFail($id);
    }

    /**
     * Insere no Banco de Dados
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request){
        $validacao = Validator::make($request->all(), [
            'name' => ['required', 'max:40'],
            'email' => ['required', 'max:50', 'email', 'unique:App\Models\Usuario,email'],
            'password' => ['required', Password::min(8)->letters()->numbers(), 'max:25'],
            'userName' => ['required', 'max:20', 'alpha_dash'],
            'zipCode' => ['required', 'string', 'size:8']
        ]);
        
        if ($validacao->fails()) {
            return back()->withInput()->withErrors($validacao);
        } else {
            $usuario = new Usuario();
            $historico = new HistoricoUsuario();

            $usuario->nome = $request->name;
            $usuario->email = $request->email;
            $historico->username = $request->userName;
            $historico->cep = $request->zipCode;
            $historico->senha = Hash::make($request->password);

            $usuario->save();
            $usuario->historico()->save($historico);
            $usuario->refresh();

            return back()->withInput()->withSuccess('Usuário cadastrado com sucesso.');
        }
    }
}

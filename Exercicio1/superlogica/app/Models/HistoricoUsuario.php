<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HistoricoUsuario extends Model{
    protected $table = 'historico_usuario';
    protected $primaryKey = 'historico_usuario_id';
    public $timestamps = false;

    protected $fillable = [
        'historico_usuario_id',
        'usuario_id',
        'username',
        'cep',
        'senha',
        'data_criacao'
    ];

    /**
     * Seta atributos
     *
     * @var array
     */
    protected $hidden = [
        'senha'
    ];

    public function usuario(){
        return $this->belongsTo('Usuario', 'usuario_id', 'usuario_id');
    }
}

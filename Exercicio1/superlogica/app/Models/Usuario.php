<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\HistoricoUsuario;

class Usuario extends Model{
    protected $table = 'usuario';
    protected $primaryKey = 'usuario_id';
    public $timestamps = false;

    /**
     * Seta atributos
     *
     * @var string[]
     */
    protected $fillable = [
        'usuario_id',
        'email',
        'nome'
    ];

    public function historico(){
        return $this->hasMany('App\Models\HistoricoUsuario', 'usuario_id', 'usuario_id')->orderBy('historico_usuario_id', 'desc');
    }

    public function ultimoHistorico(){
        return $this->hasMany('App\Models\HistoricoUsuario', 'usuario_id', 'usuario_id')->orderBy('historico_usuario_id', 'desc');
    }
}

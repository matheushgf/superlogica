<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsuarioController;

/* Agrupa rotas de usuario para prefiro de url /usuario/{rota} e
Nome de rota para usuario.{nome} */
Route::prefix('usuario')->group(function () {
    Route::name('usuario.')->group(function () {
        Route::get('/cadastro', [UsuarioController::class, 'cadastro'])->name('cadastro');
        Route::get('/lista', [UsuarioController::class, 'lista'])->name('lista');
        Route::post('/insert', [UsuarioController::class, 'insert'])->name('insert');
        Route::get('/buscaPorId/{id}', [UsuarioController::class, 'buscaPorId'])->name('buscaPorId');
    });
});

Route::get('/', function () {
    return redirect()->route('usuario.cadastro');
});
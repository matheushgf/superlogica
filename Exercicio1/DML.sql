CREATE DATABASE IF NOT EXISTS superlogica1;
USE superlogica1;

CREATE TABLE usuario(
	usuario_id INT(11) AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(50) UNIQUE,
    nome VARCHAR(40)
);

CREATE TABLE historico_usuario(
	historico_usuario_id INT(11) AUTO_INCREMENT PRIMARY KEY,
    usuario_id INT(11),
    username VARCHAR(20),
    cep VARCHAR(8),
    senha VARCHAR(70),
    data_criacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT fk_usuario_id_historico_usuario
    FOREIGN KEY(usuario_id)
    REFERENCES superlogica1.usuario(usuario_id)
    ON UPDATE RESTRICT ON DELETE RESTRICT
);
CREATE DATABASE IF NOT EXISTS superlogica;

CREATE TABLE superlogica.usuario(
	usuario_id INT(11) AUTO_INCREMENT PRIMARY KEY,
    cpf VARCHAR(12),
    nome VARCHAR(30)
);

CREATE TABLE superlogica.usuario_info(
	usuario_info_id INT(11) AUTO_INCREMENT PRIMARY KEY,
    usuario_id INT(11),
    genero CHAR(1),
    ano_nascimento YEAR,
    CONSTRAINT fk_usuario_id_usuario_info
    FOREIGN KEY(usuario_id)
    REFERENCES superlogica.usuario(usuario_id)
);

INSERT INTO superlogica.usuario(cpf, nome)
VALUES
('16798125050', 'Luke Skywalker'),
('59875804045', 'Bruce Wayne'),
('04707649025', 'Diane Prince'),
('21142450040', 'Bruce Banner'),
('83257946074', 'Harley Quinn'),
('07583509025', 'Peter Parker');

INSERT INTO superlogica.usuario_info(usuario_id, genero, ano_nascimento)
VALUES
(1, 'M', 1976),
(2, 'M', 1960),
(3, 'F', 1988),
(4, 'M', 1954),
(5, 'F', 1970),
(6, 'M', 1972);

SELECT CONCAT(u.nome, ' - ', ui.genero) AS usuario,
IF(
	(YEAR(CURRENT_DATE()) - ui.ano_nascimento) >= 50,
    'Sim',
    'Não'
) AS maior_50_anos
FROM superlogica.usuario u
JOIN superlogica.usuario_info ui ON ui.usuario_id = u.usuario_id;
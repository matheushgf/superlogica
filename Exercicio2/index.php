<?php 
    $numeros = array();

    array_push($numeros, 2);
    array_push($numeros, 3);
    array_push($numeros, 5);
    array_push($numeros, 7);
    array_push($numeros, 11);
    array_push($numeros, 13);
    array_push($numeros, 17);
    echo 'Lista populada: ';
    echo json_encode($numeros).'<br><br>';

    echo 'O número na posição 3 é ' . $numeros[2] . '<br><br>';

    $string_numeros = '2,3,5,7,6,13,17';
    $numeros_novos = explode(',', $string_numeros);
    unset($numeros);

    echo 'Nova lista: ';
    echo json_encode($numeros_novos).'<br><br>';

    if(in_array('14', $numeros_novos)){
        echo 'O número 14 está na lista';
    } else {
        echo 'O número 14 não está na lista';
    }
    echo '<br><br>';

    $ultimo = NULL;
    foreach($numeros_novos as $index => $item){
        if(isset($ultimo) && $ultimo > ((int) $item)){
            array_splice($numeros_novos, $index, 1);
        }
        $ultimo = (int) $item;
    }
    echo 'Lista atualizada: ';
    echo json_encode($numeros_novos).'<br><br>';

    array_pop($numeros_novos);
    echo 'Lista após retirada: ';
    echo json_encode($numeros_novos).'<br><br>';

    echo 'A lista contém ' . count($numeros_novos) . (count($numeros_novos) > 1 ? ' itens.' : 'item');
    echo '<br><br>';

    echo 'Lista invertida: ' . json_encode(array_reverse($numeros_novos));
?>